#   Free Project 2024 Pol Gonzalo
##   CartónGear: Falling
---

##  Autor
<b>Pol Gonzalo Méndez</b>

---

## Risketos

###  Risketos Bàsics
- [x] Controls bàsics primera persona.
- [x] Objectes interactuables: Portes, fitxer, etc.
- [x] Enemics amb State Machine.
- [x] Cameras.
- [ ] Condició victoria derrota.
### Risketos Adicionals
- [x] Mapes
- [x] Raig x per espiar a guardies (cooldown de 50s i dura 7s la habilitat)

---

## Controls
* (WASD) Moure
* (E) Interactuar
* (Q) Raig x