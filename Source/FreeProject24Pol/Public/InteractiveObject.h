
#pragma once

#include "CoreMinimal.h"
#include "Components/SphereComponent.h"
#include "Components/TextRenderComponent.h"
#include "DynamicMesh/ColliderMesh.h"
#include "GameFramework/Pawn.h"
#include "InteractiveObject.generated.h"

class APlayerCharacter;

UCLASS()
class FREEPROJECT24POL_API AInteractiveObject : public APawn
{
	GENERATED_BODY()

public:
	
	#pragma region DefaultFunctions
	
		AInteractiveObject();
		
		virtual void Tick(float DeltaTime) override;

		virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	
	#pragma endregion 

	#pragma region Variables

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Blueprintable)
			APlayerCharacter* mPlayerCharacter;
	
		UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
			USphereComponent* mSphereCollider;			

		UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
			UStaticMeshComponent* mBody;

		UPROPERTY(Blueprintable, EditDefaultsOnly, BlueprintReadWrite)
			UTextRenderComponent* mText;
	
	#pragma endregion 

	#pragma region Functions

		UFUNCTION(BlueprintCallable)
			void linkToPlayer(APlayerCharacter* PlayerCharacter);
			
			UFUNCTION(BlueprintCallable)
				void unlinkToPlayer(APlayerCharacter* PlayerCharacter);

		UFUNCTION(BlueprintCallable, Blueprintable, BlueprintImplementableEvent)
			void OnInteract();
		
	#pragma endregion 

	
protected:
	virtual void BeginPlay() override;
	

};
