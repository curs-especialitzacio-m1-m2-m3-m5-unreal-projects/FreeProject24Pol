﻿#pragma once

#define ScreenD1(x) if(GEngine) {GEngine->AddOnScreenDebugMessage(-1, 2.0f, FColor::Red, TEXT(x));}
#define Format1(x,y) FString::Printf(TEXT(x),y)
#define ScreenD2(x,y) if(GEngine) {GEngine->AddOnScreenDebugMessage(-1, 2.0f, FColor::Red, Format1(x,y));}	