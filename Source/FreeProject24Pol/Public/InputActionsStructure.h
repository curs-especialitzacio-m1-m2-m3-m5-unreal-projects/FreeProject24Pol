#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "InputActionsStructure.generated.h"

class UInputAction;

UCLASS()
class FREEPROJECT24POL_API UInputActionsStructure : public UDataAsset
{
	GENERATED_BODY()
	
public:

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		UInputAction* mInputMoveCharacter;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		UInputAction* mInputMoveCamera;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		UInputAction* mInputInteract;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		UInputAction* mInputExpose;
	
};
