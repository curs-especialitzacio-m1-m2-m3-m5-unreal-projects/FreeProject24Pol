
#pragma once

#include "CoreMinimal.h"
#include "InputActionsStructure.h"
#include "InputActionValue.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/Character.h"
#include "PlayerCharacter.generated.h"

class UInputMappingContext;

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnInteract);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnExposeEnemy);

UCLASS()
class FREEPROJECT24POL_API APlayerCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	
    #pragma region DefaultFunctions
	
		APlayerCharacter();

		virtual void Tick(float DeltaTime) override;

	void ExposeCallback();
		virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	
	#pragma endregion

	#pragma region Variables
		
		UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category=Input)
			UInputMappingContext* mInputMappingContext;

		UPROPERTY(EditDefaultsOnly,BlueprintReadWrite, Category=Input)
			UInputActionsStructure* mInputActionStructure;

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=GameCore)
			bool hasFile;
	
	#pragma endregion 

	#pragma region Functions

		#pragma region InputCallback

			void MoveCharacterCallback(const FInputActionValue& InputActionValue);

			void MoveCameraCallback(const FInputActionValue& InputActionValue);

			void InteractCallback(const FInputActionValue& InputActionValue);
	
		#pragma endregion 
	
	#pragma endregion

	#pragma region Events
	
		UPROPERTY(Blueprintable, BlueprintCallable)
			FOnInteract onInteractEvent;

		UPROPERTY(Blueprintable, BlueprintCallable, BlueprintAssignable)	
				FOnExposeEnemy onExposeEnemy;
		
	#pragma endregion 
	
protected:

	#pragma region DefaultFunctions
		virtual void BeginPlay() override;
	#pragma endregion 

	#pragma region Variables
	
		UPROPERTY(VisibleAnywhere)
			UCameraComponent* mCameraComponent;
	
	#pragma endregion

	
};
