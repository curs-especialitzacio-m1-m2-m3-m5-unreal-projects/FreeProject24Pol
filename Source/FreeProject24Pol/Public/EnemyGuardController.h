#include "AIController.h"
#include "EnemyGuardController.generated.h"

UENUM()
enum EnemyState { 
	PATROL=0,
	SEARCHING=1,
	FOLLOW=2,
	ATTACK=3
};

DECLARE_MULTICAST_DELEGATE_OneParam(FRevealPositionPlayerEvent, FVector);

UCLASS()
class FREEPROJECT24POL_API AEnemyGuardController : public AAIController
{
	GENERATED_BODY()
	
};
