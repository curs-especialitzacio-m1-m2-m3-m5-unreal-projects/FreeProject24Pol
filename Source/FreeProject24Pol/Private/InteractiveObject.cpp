#include "InteractiveObject.h"

#include "PlayerCharacter.h"

AInteractiveObject::AInteractiveObject()
{
	PrimaryActorTick.bCanEverTick = true;

	if(mSphereCollider==nullptr)
	{
		mSphereCollider=CreateDefaultSubobject<USphereComponent>(TEXT("Sphere"));
		mSphereCollider->SetSphereRadius(110.f);
		mSphereCollider->SetCollisionProfileName("Trigger");
	}
	
	
	FAttachmentTransformRules AttachRules {EAttachmentRule::SnapToTarget, true};
	if(mBody==nullptr)
	{
		mBody=CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Body"));
		mBody->AttachToComponent(mSphereCollider, AttachRules, FName("Body"));
		mBody->SetupAttachment(GetRootComponent());
	}
	
	if(mText==nullptr)
	{
		mText= CreateDefaultSubobject<UTextRenderComponent>(TEXT("Text"));
		mText->SetVisibility(false);
		mText->AttachToComponent(mSphereCollider, AttachRules, FName("Text"));	
	}
	
}

void AInteractiveObject::BeginPlay()
{
	Super::BeginPlay();
	mText->SetVisibility(false);
}

void AInteractiveObject::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if(mPlayerCharacter)
	{
		FRotator diff = mPlayerCharacter->GetActorRotation();
		diff.Yaw+=180;
		mText->SetRelativeRotation(diff);
	}
}

void AInteractiveObject::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	
}

void AInteractiveObject::linkToPlayer(APlayerCharacter* PlayerCharacter)
{
	if(PlayerCharacter!=nullptr)
	{
		PlayerCharacter->onInteractEvent.AddDynamic(this, &AInteractiveObject::OnInteract);
		mPlayerCharacter=PlayerCharacter;
		mText->SetVisibility(true);
	}
}

void AInteractiveObject::unlinkToPlayer(APlayerCharacter* PlayerCharacter)
{
	if(PlayerCharacter!=nullptr)
	{
		PlayerCharacter->onInteractEvent.RemoveDynamic(this, &AInteractiveObject::OnInteract);
		mPlayerCharacter=nullptr;
		mText->SetVisibility(false);
	}
}

