

#include "PlayerCharacter.h"
#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"
#include "FreeProject24Pol/Public/Utils.h"

APlayerCharacter::APlayerCharacter()
{
	PrimaryActorTick.bCanEverTick = true;

	mCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("mCameraComponent"));
	mCameraComponent->SetupAttachment(GetMesh());
	mCameraComponent->bUsePawnControlRotation=true;

	
	
}

void APlayerCharacter::BeginPlay()
{
	Super::BeginPlay();
	hasFile = false;
}

void APlayerCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void APlayerCharacter::ExposeCallback()
{
	if(IsValid(Controller))
	{
		onExposeEnemy.Broadcast();
	}
}

void APlayerCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	
	check(PlayerInputComponent);
	
	APlayerController* mPlayerController = Cast<APlayerController>(GetController());
	UEnhancedInputLocalPlayerSubsystem* EInputSubsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(mPlayerController->GetLocalPlayer());
	EInputSubsystem->ClearAllMappings();
	EInputSubsystem->AddMappingContext(mInputMappingContext, 0);

	UEnhancedInputComponent* EInputComponent {Cast<UEnhancedInputComponent>(PlayerInputComponent)};
	
	EInputComponent->BindAction(mInputActionStructure->mInputMoveCharacter, ETriggerEvent::Triggered, this, &APlayerCharacter::MoveCharacterCallback);
	EInputComponent->BindAction(mInputActionStructure->mInputMoveCamera, ETriggerEvent::Triggered, this, &APlayerCharacter::MoveCameraCallback);
	EInputComponent->BindAction(mInputActionStructure->mInputInteract, ETriggerEvent::Triggered, this, &APlayerCharacter::InteractCallback);
	EInputComponent->BindAction(mInputActionStructure->mInputExpose, ETriggerEvent::Triggered, this, &APlayerCharacter::ExposeCallback);

	
}

void APlayerCharacter::MoveCharacterCallback(const FInputActionValue& InputActionValue)
{
	if(IsValid(Controller))
	{
		const FVector direction { InputActionValue.Get<FVector>() };
		const FRotator moveRotator { 0, Controller->GetControlRotation().Yaw, 0 };

		if (direction.Y != 0.f)
		{
			const FVector dir { moveRotator.RotateVector(FVector::ForwardVector)};
			AddMovementInput(dir, direction.Y);
		}
		if (direction.X!=0.f)
		{
			const FVector dir {moveRotator.RotateVector(FVector::RightVector)};
			AddMovementInput(dir, direction.X);
		}
	}
}

void APlayerCharacter::MoveCameraCallback(const FInputActionValue& InputActionValue)
{
	if(IsValid(Controller))
	{
		const FVector direction { InputActionValue.Get<FVector>() };
		
		if(direction.X!=0.f)
		{
			AddControllerYawInput(direction.X);
		}

		if(direction.Y<0.f)
		{	
			AddControllerPitchInput(abs(direction.Y));
		}
		else if(direction.Y>0.f)
		{
			AddControllerPitchInput(-direction.Y);
		}
		
	}
}

void APlayerCharacter::InteractCallback(const FInputActionValue& InputActionValue)
{
	if(IsValid(Controller))
	{
		onInteractEvent.Broadcast();
	}
}

